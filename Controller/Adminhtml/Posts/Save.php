<?php
namespace Os\OlNews\Controller\Adminhtml\Posts;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Escaper;
use Os\OlNews\Model\Post;
use Os\OlNews\Model\Post\ImageUploader;

class Save extends Action
{
    private $escaper;
    private $imageUploader;

    public function __construct(
        Context $context,
        Escaper $escaper,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->escaper = $escaper;
        $this->imageUploader = $imageUploader;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create(Post::class);
            $post = [];
            foreach ($model->getEscapeFields() as $postField) {
                if (isset($data['olnews_posts'][$postField])) {
                    $post[$postField] = $this->escaper->escapeHtml($data['olnews_posts'][$postField]);
                }
            }

            $message = 'Added new post.';
            $idFieldName = $model->getIdFieldName();
            if (isset($data['olnews_posts'][$idFieldName])) {
                $post[$idFieldName] = $data['olnews_posts'][$idFieldName];
                $message = 'Updated post.';
            }

            $imageName = '';
            $post['image'] = $data['olnews_posts']['img'][0]['name'] ??
                $data['olnews_posts']['image'][0]['name'];

            $model->setData($post);

            try {
                $model->save();
                if ($imageName) {
                    $this->imageUploader->moveFileFromTmp($imageName);
                }
                $this->messageManager->addSuccessMessage(__($message));
                return $resultRedirect->setPath('*/*/index');
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the new post.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        $this->messageManager->addErrorMessage(__('There is no data to save.'));
        return $resultRedirect->setPath('*/*/index');
    }
}
