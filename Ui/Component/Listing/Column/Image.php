<?php
namespace Os\OlNews\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Os\OlNews\Model\Post;

class Image extends Column
{
    const URL_PATH_EDIT = 'news/posts/edit';
    private $post;
    private $urlBuilder;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        Post $post,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->post = $post;
        $this->urlBuilder = $urlBuilder;
    }

    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $post = new \Magento\Framework\DataObject($item);
                $imageUrl = $this->post->getImageUrl($post['image']);
                if ($imageUrl) {
                    $item[$fieldName . '_src'] = $imageUrl;
                    $item[$fieldName . '_orig_src'] = $imageUrl;
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                        self::URL_PATH_EDIT,
                        ['id' => $post['id']]
                    );
                    $item[$fieldName . '_alt'] = $post['name'];
                }
            }
        }
        return $dataSource;
    }
}
