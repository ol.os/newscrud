<?php
namespace Os\OlNews\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class PostActions extends Column
{

    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->getContext()->getUrl(
                                'news/posts/edit',
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Edit')
                        ],
                        'delete' => [
                            'href' => $this->getContext()->getUrl(
                                'news/posts/delete',
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete'),
                                'message' => __('Are you sure you want to delete the post "'.$item['title'].'"?')
                            ]
                        ]
                    ];
                }
            }
        }
        return $dataSource;
    }
}
