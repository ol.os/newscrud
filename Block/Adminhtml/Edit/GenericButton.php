<?php
namespace Os\OlNews\Block\Adminhtml\Edit;

use Magento\Backend\Block\Widget\Context;

class GenericButton
{
    protected $urlBuilder;
    protected $actionName;

    public function __construct(Context $context)
    {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->actionName = $context->getRequest()->getActionName();

    }

    public function getActionName()
    {
        return $this->actionName;
    }

    public function getUrl($route = '', $params = []): string
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
