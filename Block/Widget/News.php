<?php
namespace Os\OlNews\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;
use Os\OlNews\Model\Post as PostModel;
use Os\OlNews\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;

class News extends Template implements BlockInterface
{
    protected $_template = "widget/posts.phtml";

    private $postCollectionFactory;
    private $postModel;

    public function __construct(
        Context $context,
        PostModel $postModel,
        PostCollectionFactory $postCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->postCollectionFactory = $postCollectionFactory;
        $this->postModel = $postModel;
    }

    public function getPosts()
    {
        $postCollection = $this->postCollectionFactory->create();
        foreach ($postCollection as $post) {
            $post->setImage($this->postModel->getImageUrl($post['image']));
        }
        $postCollection->addFieldToSelect('*')->load();

        return $postCollection->getItems();
    }
}
