<?php

namespace Os\OlNews\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Os\OlNews\Model\ResourceModel\Post as ResourcePost;
use Os\OlNews\Model\Post\ImageFile;

class Post extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'olnews_posts';
    protected $_cacheTag = 'olnews_posts';
    protected $_eventPrefix = 'olnews_posts';
    protected $storeManager;

    protected function _construct()
    {
        $this->_init(ResourcePost::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getEscapeFields()
    {
        return [
            'title',
            'description'
        ];
    }

    public function getDefaultValues()
    {
        return [];
    }

    public function getImageUrl(?string $imageName): ?string
    {
        $urlPathMedia = $this->getStoreManager()->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $fileDriver = ObjectManager::getInstance()->get(\Magento\Framework\Filesystem\Driver\File::class);
        $dirPathMedia = ObjectManager::getInstance()->get(\Magento\Framework\App\Filesystem\DirectoryList::class)->getPath('media') . '/';

        $imageAbsolutePath = ImageFile::IMAGE_PATH .'/'. $imageName;
        if (!$imageName || !$fileDriver->isExists($dirPathMedia.$imageAbsolutePath)) {
            return $urlPathMedia . ImageFile::IMAGE_PATH . '/no_image.png' ;
        }

        return $urlPathMedia . $imageAbsolutePath;
    }

    private function getStoreManager()
    {
        if (!isset($this->storeManager)) {
            $this->storeManager = ObjectManager::getInstance()->get(StoreManagerInterface::class);
        }
        return $this->storeManager;
    }
}
