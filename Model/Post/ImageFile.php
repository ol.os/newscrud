<?php
namespace Os\OlNews\Model\Post;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Mime;
use Magento\Framework\Filesystem;

class ImageFile
{
    /**
     * Path in /pub/media directory
     */
    const IMAGE_TEMP_PATH = 'olnews/tmp/posts';
    const IMAGE_PATH = 'olnews/posts';
    const IMAGE_ALLOWED_EXTENSIONS = ['jpg', 'jpeg', 'gif', 'png'];
    private $filesystem;
    private $mime;
    private $mediaDirectory;

    public function __construct(
        Filesystem $filesystem,
        Mime $mime
    ) {
        $this->filesystem = $filesystem;
        $this->mime = $mime;
    }

    private function getMediaDirectory()
    {
        if ($this->mediaDirectory === null) {
            $this->mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        }
        return $this->mediaDirectory;
    }

    public function getMimeType($fileName)
    {
        $filePath = self::IMAGE_PATH . '/' . ltrim($fileName, '/');
        $absoluteFilePath = $this->getMediaDirectory()->getAbsolutePath($filePath);

        return $this->mime->getMimeType($absoluteFilePath);
    }

    public function isExist($fileName, $baseTmpPath = false)
    {
        $filePath = self::IMAGE_PATH . '/' . ltrim($fileName, '/');
        if ($baseTmpPath) {
            $filePath = $baseTmpPath . '/' . ltrim($fileName, '/');
        }

        return $this->getMediaDirectory()->isExist($filePath);
    }

    public function deleteFile($fileName, $baseTmpPath = false)
    {
        $filePath = self::IMAGE_PATH . '/' . ltrim($fileName, '/');
        if ($baseTmpPath) {
            $filePath = $baseTmpPath . '/' . ltrim($fileName, '/');
        }

        return $this->getMediaDirectory()->delete($filePath);
    }
}
