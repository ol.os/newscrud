<?php
namespace Os\OlNews\Model\Post;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Os\OlNews\Model\ResourceModel\Post\CollectionFactory;

class DataProvider extends AbstractDataProvider
{

    private $loadedData = null;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $postCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $postCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }


    public function getData(): array
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $this->loadedData = [];
        foreach ($this->collection->getItems() as $post) {
            $this->loadedData[$post->getId()]['olnews_posts'] = $post->getData();
//            $this->loadedData[$post->getId()]['olnews_posts']['img'] = $post->getImage();
        }

        return $this->loadedData;
    }
}
